# p3ll3

Photo archiver on https://p3ll3.eu written in Python with Django framework.

## Requirements
### Server:
- Python >= 3.7
- MySQL server
- Django 3.0
- mysqlclient
- sorl-thumbnail >= 12
- Pillow
- pyexiftool
- exiftool

### Client:
- Browser support for JS ES6

