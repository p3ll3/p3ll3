from django import forms
from .models import Evento, EventoSelect, File, Keyword, Autore, Categoria, Famiglia
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.forms import ModelMultipleChoiceField

from datetime import datetime

YEARS = range(2000, datetime.now().year + 1)

class EventoForm(forms.ModelForm):
    class Meta:
        model = Evento
        fields = [
            'data_inizio',
            'data_multi',
            'data_fine',
            'nome',
            'luogo',
            'comune',
            'categorie',
            'descrizione',
            'famiglie'
        ]
        labels = {
            'data_inizio': 'Data',
            'data_multi': 'Più Giorni',
            'data_fine': 'Data Fine',
        }
        widgets = {
            'data_inizio': forms.SelectDateWidget(years=YEARS),
            'data_fine': forms.SelectDateWidget(years=YEARS),
            'descrizione': forms.Textarea()
        }
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(EventoForm, self).__init__(*args, **kwargs)
        self.fields['famiglie'].queryset = Famiglia.objects.filter(users=self.user)


class EventoSelectForm(forms.ModelForm):
    class Meta:
        model = File
        fields = [
            'evento'
        ]
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(EventoSelectForm, self).__init__(*args, **kwargs)
        self.fields['evento'].choices = EventoSelect(self.user).items()


class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ['nome']


class AutoreForm(forms.ModelForm):
    class Meta:
        model = Autore
        fields = ['nome']


class KeywordForm(forms.ModelForm):
    class Meta:
        model = Keyword
        fields = ['nome']


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = [
            'file',
            'evento'
        ]
        widgets= {
            'file': forms.FileInput(attrs={'multiple':True}),
            'evento': forms.HiddenInput()
        }


class CercaForm(forms.Form):
    q = forms.CharField(max_length=50)


class DeleteForm(forms.Form):
    delete = forms.BooleanField()


class UserChangeForm(UserChangeForm):
    famiglie = ModelMultipleChoiceField(queryset=Famiglia.objects.all())
    
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'famiglie', 'groups']
    
    def save(self, commit=True):
        f = super(UserChangeForm, self).save(commit=False)
        
        f.famiglia_set.set(self.cleaned_data.get('famiglie'))
        
        if commit:
            self.save_m2m()
            f.save()
            
        return f


class FamigliaForm(forms.ModelForm):
    class Meta:
        model = Famiglia
        exclude = []
