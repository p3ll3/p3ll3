from django.views import View
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, get_list_or_404, render, redirect
from django.core.files.storage import default_storage
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, F, Count, CharField, Value as V
from django.db.models.functions import Chr, Concat
from django.utils.text import slugify
from django.utils.decorators import method_decorator

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import permission_required

from .models import File, Evento, EventoSelect, Keyword, Categoria, Autore, Famiglia
from .forms import EventoSelectForm, EventoForm, FileForm, DeleteForm, KeywordForm, AutoreForm, CategoriaForm, UserChangeForm, FamigliaForm

from io import BytesIO
from zipfile import ZipFile
from urllib.parse import unquote
from itertools import chain
import os, logging, datetime, copy #asyncio

logger = logging.getLogger(__name__)


class LoginRequiredMixin(LoginRequiredMixin):
    def handle_no_permission(self):
        if self.request.is_ajax():
            return JsonResponse({
                 'return': -1,
                 'error': "Non Autenticato"
             }, status=401)
        
        return super().handle_no_permission()    

class LogoutReqiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_anonymous
    
    def handle_no_permission(self):
        return redirect('/')


views = {
    'e': {
        'm': Evento,
        'u': 'famiglie__users__pk',
        'f': 'evento',
        'fs': 'evento',
        'sorting': '-data_inizio',
        'count': 'file'
    },
    'p': {
        'm': Keyword,
        'u': 'file__evento__famiglie__users__pk',
        'f': 'keyword',
        'fs': 'keywords',
        'sorting': '-count',
        'count': 'file'
    },
    'a': {
        'm': Autore,
        'u': 'file__evento__famiglie__users__pk',
        'f': 'autore',
        'fs': 'autore',
        'sorting': '-count',
        'count': 'file'
    },
    'c': {
        'm': Categoria,
        'u': 'evento__famiglie__users__pk',
        'f': 'evento__categorie',
        'fs': 'evento__categorie',
        'sorting': '-count',
        'count': 'evento__file'
    },
    'f': {
        'm': Famiglia,
        'u': 'evento__famiglie__users__pk',
        'f': 'evento__famiglie',
        'fs': 'evento__famiglie'
    }
}


def handleEdit(model, model_form, request, id, model_name, model_f, form_args = {}, v = None):
    filters = {views[v]['u']: request.user.id} if v else {}
    filters['pk'] = id
    data = get_object_or_404(model.objects.filter(**filters).distinct())
    
    if 'delete' in request.POST:
        form = DeleteForm(request.POST)
        
        if form.is_valid():
            logger.info("Delete " + model_name + " OK: (" + str(data.id) + ") " + str(getattr(data, model_f)))
            data.delete()
            return JsonResponse({'return': 1, 'errors': {}})
        
        logger.info("Delete " + model_name + " BAD: (" + str(data.id) + ") " + str(getattr(data, model_f)) + "; ERRORS: " + str(form.errors.get_json_data()))
        return JsonResponse({'return': -1, 'errors': form.errors.get_json_data()})
    else:
        form = model_form(request.POST, **form_args, instance=data)
        
        if form.is_valid():
            new = form.save()
            logger.info("Edit " + model_name + " OK: (" + str(new.id) + ") " + str(getattr(new, model_f)))
            return JsonResponse({'return': 1, 'id': new.id, 'errors': {}})
        
        logger.info("Edit " + model_name + " BAD: (" + str(data.id) + ") " + str(getattr(data, model_f)) + "; ERRORS: " + str(form.errors.get_json_data()))
        return JsonResponse({'return': -1, 'errors': form.errors.get_json_data()})


def handleCreate(model_form, request, model_name, model_f, model_err, form_args = {}):
    form = model_form(request.POST, **form_args)
    
    if form.is_valid():
        new = form.save()
        logger.info("Create " + model_name + " OK: (" + str(new.id) + ") " + str(getattr(new, model_f)))
        return JsonResponse({'return': 1, 'id': new.id, 'nome_f': getattr(new, model_f), 'errors': {}})
    
    logger.info("Create " + model_name + " BAD: " + str(form.cleaned_data.get(model_err)) + "; ERRORS: " + str(form.errors.get_json_data()))
    return JsonResponse({'return': -1, 'errors': form.errors.get_json_data()})


def handleGet(model, request, id, count, v = None):
    filters = {views[v]['u']: request.user.id} if v else {}
    filters['pk'] = id
    data = get_object_or_404(model.objects.filter(**filters).distinct().annotate(count=Count(count, distinct=True)))
    return JsonResponse(data.as_dict(), safe=False)


def handleList(model, request, filter, count, sorting, v = None, c = None):
    start = int(request.GET['s']) if 's' in request.GET and request.GET['s'] != '' else 0
    limit = int(request.GET['l']) if 'l' in request.GET and request.GET['l'] != '' else 25
    data = model.objects
    data = data.filter(**{views[v]['u']: request.user.id}) if v and not c else data
    
    if 'q' in request.GET and request.GET['q'] != '':
        query = str(request.GET['q'])
        
        if isinstance(filter, list):
            filters = Q()
            for f in filter:
                filters.add(Q(**{f: query}), Q.OR)
        else:
            filters = Q(**{filter: query})
        
        data = data.filter(filters)
    else:
        data = data.all()
    
    filter = Q()
    
    if c:
        filter.add(Q(**{views[v]['u']: request.user.id}), Q.AND)
    
        if type(c) == dict:
            filter.add(Q(**c), Q.AND)
    
    data = data.annotate(count=Count(count, filter=filter, distinct=True))
    count = data.count()
    data = data.order_by(sorting)[start:limit]
    
    data_list = []
    data_list.append({'count': count, 'start': start, 'limit': limit, 'more': count-limit})
    
    for d, v in zip(data, data.values()):
        d = d.as_dict()
        data_list.append(d)
    
    return JsonResponse(data_list, safe=False)



class IndexView(LoginRequiredMixin, View):
    def get(self, request, v = '', id = ''):
        context, nome, msg = {}, '',  ''
        
        if request.user.has_perm('ph.view_evento'):
            context['cp_eventi'] = {
                'id': 'cp-eventi',
                'header': 'Eventi',
                'obj': 'eventi',
                'view_type': 'e',
                'view_nome': 'Eventi',
                'noresult': 'Nessun Evento Trovato',
                'create': 'Nuovo Evento',
            }
            context['cp_eventi_fields'] = {
                'data centered': 'Data',
                'nome overflowed': 'Nome',
                'luogo_f overflowed': 'Luogo',
                'categorie_f overflowed': 'Categorie'
            }
            context['cp_eventi_controls'] = ['count', 'download', 'filtra']
            
            if request.user.has_perm('ph.change_evento'):
                context['cp_eventi']['edit'] = True
                context['form_evento'] = EventoForm(user=request.user.id)
                context['cp_eventi_controls'].extend(['create', 'edit'])
        
        if request.user.has_perm('ph.add_file'):
            context['cp_upload'] = {
                'id': 'cp-upload',
                'header': 'Upload',
                'obj': 'upload'
            }
            context['form_evento_select'] = EventoSelectForm(auto_id='id_%s_select', user=request.user.id)
            context['form_file'] = FileForm()
        
        if request.user.has_perm('ph.view_keyword'):
            context['cp_parole'] = {
                'id': 'cp-parole',
                'header': 'Parole',
                'obj': 'parole',
                'view_type': 'p',
                'view_nome': 'Parole',
                'noresult': 'Nessuna Parola Trovata',
                'create': 'Nuova Parola'
            }
            context['cp_parole_fields'] = {
                'nome justified': 'Nome',
            }
            context['cp_parole_controls'] = ['count', 'download', 'filtra']
            
            if request.user.has_perm('ph.change_keyword'):
                context['cp_parole']['edit'] = True
                context['form_keyword'] = KeywordForm()
                context['cp_parole_controls'].extend(['create', 'edit'])
        
        if request.user.has_perm('ph.view_categoria'):
            context['cp_categorie'] = {
                'id': 'cp-categorie',
                'header': 'Categorie',
                'obj': 'categorie',
                'view_type': 'c',
                'view_nome': 'Categorie',
                'noresult': 'Nessuna Categoria Trovata',
                'create': 'Nuova Categoria'
            }
            context['cp_categorie_fields'] = {
                'nome justified': 'Nome',
            }
            context['cp_categorie_controls'] = ['count', 'download', 'filtra']
            
            if request.user.has_perm('ph.change_categoria'):
                context['cp_categorie']['edit'] = True
                context['form_categoria'] = CategoriaForm()
                context['cp_categorie_controls'].extend(['create', 'edit'])
        
        if request.user.has_perm('ph.view_autore'):
            context['cp_autori'] = {
                'id': 'cp-autori',
                'header': 'Autori',
                'obj': 'autori',
                'view_type': 'a',
                'view_nome': 'Autori',
                'noresult': 'Nessun Autore Trovato',
                'create': 'Nuovo Autore'
            }
            context['cp_autori_fields'] = {
                'nome justified': 'Nome'
            }
            context['cp_autori_controls'] = ['count', 'download', 'filtra']
            
            if request.user.has_perm('ph.change_autore'):
                context['cp_autori']['edit'] = True
                context['form_autore'] = AutoreForm()
                context['cp_autori_controls'].extend(['create', 'edit'])
        
        if request.user.has_perm('ph.view_famiglia'):
            context['cp_famiglie'] = {
                'id': 'cp-famiglie',
                'header': 'Famiglie',
                'obj': 'famiglie',
                'view_type': 'f',
                'view_nome': 'Famiglie',
                'noresult': 'Nessuna Famiglia Trovato',
                'create': 'Nuovo Famiglia'
            }
            context['cp_famiglie_fields'] = {
                'nome justified': 'Nome',
                'users_f overflowed': 'Utenti'
            }
            context['cp_famiglie_controls'] = ['count']
            
            if request.user.has_perm('ph.change_famiglia'):
                context['cp_famiglie']['edit'] = True
                context['form_famiglia'] = FamigliaForm()
                context['cp_famiglie_controls'].extend(['create', 'edit'])
        
        if request.user.has_perm('ph.view_user'):
            context['cp_utenti'] = {
                'id': 'cp-utenti',
                'header': 'Utenti',
                'obj': 'utenti',
                'noresult': 'Nessun Utente Trovato',
                'create': 'Nuovo Utente'
            }
            context['cp_utenti_fields'] = {
                'username centered': 'Username',
                'first_name': 'Nome',
                'last_name': 'Cognome',
                'famiglie_f centered overflowed': 'Famiglie',
                'groups_f overflowed': 'Gruppi',
            }
            context['cp_utenti_controls'] = []
            
            if request.user.has_perm('ph.change_user'):
                context['cp_utenti']['edit'] = True
                context['form_utente_create'] = UserCreationForm()
                context['form_utente_edit'] = UserChangeForm()
                context['cp_utenti_controls'].extend(['create', 'edit'])
        
        if v in views and id:
            try:
                nome = str(views[v]['m'].objects.filter(**{views[v]['u']: request.user.id, 'id': id}).distinct().first())
            except ObjectDoesNotExist:
                v, id, nome, msg = '', '', '', -1
        
        context['view_type'] = v
        context['view_id'] = id
        context['view_nome'] = nome
        context['view_msg'] = msg
        
        return render(request, 'index.html', context)


class LoginView(LogoutReqiredMixin, View):
    def get(self, request):
        login_form = AuthenticationForm()
        return render(request, 'login.html', {"login_form": login_form})
    
    def post(self, request):
        form = AuthenticationForm(request=request, data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            
            if user is not None:
                login(request, user)
                logger.info("Login OK: " + user.username)
                return JsonResponse({'return': 1})
        
        logger.info("Login BAD: " + form.cleaned_data.get('username') + "; ERRORS: " + str(form.errors.get_json_data()))
        return JsonResponse({'return': -1, 'errors': form.errors.get_json_data()})


class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        logger.info("Logout: " + request.user.username)
        return redirect('/login/')


class SearchView(LoginRequiredMixin, View):
    def get(self, request):
        if not 'q' in request.GET or request.GET['q'] == '':
            return JsonResponse(list(), safe=False)
        
        q = request.GET['q']
        qlist = []
        
        for l in q.split("&&"):
            qlist.append(l.strip())
        
        start, limit = 0, 8
        views_s = copy.deepcopy(views)
        views_s.pop('f')
        data = []
        
        if 'v' in request.GET and request.GET['v'] in views_s:
            view = request.GET['v']
            views_s = {view: views_s[view]}
        else:
            d = File.objects.filter(evento__famiglie__users__pk=request.user.id)
            
            for l in qlist:
                d = d.filter(keywords__nome__icontains=l.strip())
            
            if len(qlist) > 0:
                data.insert(0, [{
                    'id': " && ".join(qlist),
                    'nome_f': " && ".join(qlist),
                    'view': '',
                    'count': d.count()
                }])
            # data.append({
            #     'id': " && ".join(qlist),
            #     'nome_f': " && ".join(qlist),
            #     'view': '',
            #     'count': d.count()
            # })
        for v, m in views_s.items():
            d = m['m'].objects.filter(**{views_s[v]['u']: request.user.id}).annotate(
                count=Count('id'),
                nome_f=F('nome') if v != "e" else Concat('data_inizio', V(': '), 'nome', output_field=CharField()),
                view=Chr(ord(v), output_field=CharField())
            ).values('id', 'nome_f', 'view', 'count').order_by(views_s[v]['sorting'])
            
            if len(qlist) > 0:
                for l in qlist:
                    d = d.filter(**{'nome__icontains': l})
            
            data.append(d[start:limit])
            
            # data.append(m['m'].objects.filter(**{views_s[v]['u']: request.user.id, 'nome__icontains': q[0]}).annotate(
            #     count=Count('id'),
            #     nome_f=F('nome') if v != "e" else Concat('data_inizio', V(': '), 'nome', output_field=CharField()),
            #     view=Chr(ord(v), output_field=CharField())
            # ).values('id', 'nome_f', 'view', 'count').order_by(sorting)[start:limit])
        
        
        # if len(qlist) > 1:
        #     data = File.objects.filter(evento__famiglie__users__pk=request.user.id)
        #     qlistf = []
        #     
        #     for l in qlist:
        #         data = data.filter(keywords__nome__icontains=l.strip())
        #         qlistf.append(l.strip())
        #     
        #     data = [{
        #         'id': " && ".join(qlistf),
        #         'nome_f': " && ".join(qlistf),
        #         'view': '',
        #         'count': data.count()
        #     }]
        data = list(chain(*data))
        
        # if len(qlist) > 1:
        #     data.insert(0, {
        #         'id': " && ".join(qlist),
        #         'nome_f': " && ".join(qlist),
        #         'view': view,
        #         'count': len(data)
        #     })
        
        
        
        return JsonResponse(data[start:limit], safe=False)


class FilesView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_file', raise_exception=True))
    def get(self, request):
        start = int(request.GET['s']) if 's' in request.GET and request.GET['s'] != '' else 0
        limit = int(request.GET['l']) if 'l' in request.GET and request.GET['l'] != '' else 25
        view = str(request.GET['v']) if 'v' in request.GET and request.GET['v'] != '' else None
        thumb = str(request.GET['t']) if 't' in request.GET and request.GET['t'] != '' else "x200"
        query = unquote(str(request.GET['q'])) if 'q' in request.GET and request.GET['q'] != '' else None
        
        filters = Q()
        qlist = []
        views_s = copy.deepcopy(views)
        views_s.pop('f')
        
        if query is not None:
            qlist = query.split("&&")
        
        if view in views_s and view not in request.GET:
            sorting = views[view]['sorting']
            model = views[view]['m']
            annotate = {'count': Count(views[view]['count'], distinct=True)}
            cover = True
            qFilter = 'nome__icontains'
            
            # if len(qlist) == 1:
            #     filters.add(Q(nome__icontains=query), Q.AND)
            
            #views[view]['m'].objects.filter(**filters).distinct().annotate(conto=Count(views[view]['conto'], distinct=True))
        
        else:
            model = File
            sorting = '-datetime'
            cover = False
            annotate = {}
            filters.add(Q(meta=True), Q.AND)
            filters.add(Q(evento__famiglie__users__pk=request.user.id), Q.AND)
            qFilter = 'keywords__nome__icontains'
            
            if 'e' in request.GET and request.GET['e'] != '':
                filters.add(Q(evento=int(request.GET['e'])), Q.AND)
                sorting = 'datetime'
            
            if 'p' in request.GET and request.GET['p'] != '':
                #model = model.objects.filter(keywords=int(request.GET['p']))
                filters.add(Q(keywords=int(request.GET['p'])), Q.AND)
            
            if 'a' in request.GET and request.GET['a'] != '':
                filters.add(Q(autore=int(request.GET['a'])), Q.AND)
            
            if 'c' in request.GET and request.GET['c'] != '':
                filters.add(Q(evento__categorie=int(request.GET['c'])), Q.AND)
            
            # if len(qlist) == 1:
            #     filters.add(Q(keywords__nome__icontains=query), Q.AND)
            
            #data = model.objects.filter(filters).distinct()
            
            #count = data.count()
            #data = data.order_by(sorting)[start:limit]
        
        data = model.objects.filter(filters).distinct().annotate(**annotate)
        
        if query is not None and len(qlist) > 0:
            for l in qlist:
                data = data.filter(**{qFilter: l.strip()}).distinct()
        
        count = data.count()
        data = data.order_by(sorting)[start:limit]
        
        data_list = []
        data_list.append({'count': count, 'start': start, 'limit': limit, 'more': count-limit})
        
        if cover:
            for d in data:
                cover = list(File.objects.filter(**{views[view]['fs']: d.id, 'evento__famiglie__users__pk': request.user.id}).distinct().order_by("datetime")[0:4].values_list("id", flat=True))
                data_list.append(d.as_dict(cover))
        else:
            for d in data:
                data_list.append(d.as_dict(thumb=thumb))
        
        return JsonResponse(data_list, safe=False)
    
    @method_decorator(permission_required('ph.add_file', raise_exception=True))
    def post(self, request):
        form = FileForm(request.POST, request.FILES)
        
        if form.is_valid():
            files = request.FILES.getlist('file')
            f_list = []
            
            for f in files:
                new_file = File(file=f, evento=form.cleaned_data['evento'])
                new_file.save()
                # logger.info("Upload File: (" + str(new_file.id) + ") " + new_file.file.name)
                # #asyncio.run(new_file.set_meta())
                # new_file.set_meta()
                #new_file.save()
                f_list.append(new_file.id)
            
            logger.info("Upload File OK: " + str(f_list))
            return JsonResponse({'return': 1, 'files': f_list})
        
        logger.info("Upload File BAD: " + str(form) + "; ERRORS: " + str(form.errors.get_json_data()))
        return JsonResponse({'return': -1, 'errors': form.errors.get_json_data()})


class FileView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_file', raise_exception=True))
    def get(self, request, id):
        if id != 0:
            file = get_object_or_404(File.objects.select_related().distinct(), pk=id, evento__famiglie__users__pk=request.user.id)
            
            if 's' in request.GET:
                data = file.as_dict()
                data["meta"] = file.meta_dict()
                return JsonResponse(data)
            
            if 'w' in request.GET:
                name = os.path.splitext(os.path.basename(file.file.name))[0] + "-WEB.jpg"
                out = file.webversion(request.GET['w'])
                mime = 'image/jpeg'
                disposition = 'attachment; '
                logger.info("Download File: (" + str(file.id) + ") " + str(file.file.name) + " (WEB)")
            else:
                if 't' in request.GET:
                    crop = 'noop'
                    
                    if request.GET['t'] == 'cover':
                        size = '100x100'
                        crop = 'center'
                    else:
                        size = request.GET['t']
                    
                    name = file.thumbnail(size, crop).name
                    mime = 'image/jpeg'
                    disposition = 'inline; '
                else:
                    name = file.file.name
                    mime = file.mime
                    disposition = 'attachment; '
                    logger.info("Download File: (" + str(file.id) + ") " + str(file.file.name) + " (PRESS)")
    
                out = default_storage.open(name, 'rb')
                name = os.path.basename(name)
        elif 'z' in request.GET:
            z = request.GET['z']
            
            if 'v' in request.GET:
                v = request.GET['v']
                filter = {views[v]['fs']: int(z)}
            else:
                v = None
                filter = {'pk__in': z.split(';')}
            
            filter['evento__famiglie__users__pk'] = request.user.id
            
            files = get_list_or_404(File, **filter)
            byte = BytesIO()
            zf = ZipFile(byte, 'a')
            i = 0
            
            if 'w' in request.GET:
                for f in files:
                    zf.writestr(os.path.basename(f.file.name), f.webversion(request.GET['w']))
                    i += 1
            else:
                for f in files:
                    zf.write(f.file.path, os.path.basename(f.file.name))
                    i += 1
            
            zf.close()
            
            if v == 'e':
                name = slugify(getattr(f, views[v]['f']))
            else:
                if v:
                    if v == 'p':
                        name = slugify(get_object_or_404(Keyword, pk=int(z))) + '-'
                    elif v == 'c':
                        name = slugify(get_object_or_404(Categoria, pk=int(z))) + '-'
                    elif v == 'a':
                        name = slugify(getattr(f, views[v]['f'])) + '-'
                else:
                    name = ''
                
                name = datetime.datetime.now().strftime('%Y%m%d-%H%M%S-') + name + str(i) + '_files'
            
            if 'w' in request.GET:
                name  += '-WEB'
            
            name += '.zip'
            mime = 'application/x-zip-compressed'
            disposition = 'attachment; '
            out = byte.getvalue()
            logger.info("Download ZIP: " + str(name))
            
        response = HttpResponse(out, content_type=mime)
        response['Content-Disposition'] = disposition + 'filename=' + name
        return response


class EventiView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_evento', raise_exception=True))
    def get(self, request):
        if 'select' in request.GET:
            return JsonResponse(EventoSelect(request.user.id))
            
        filter = [
            'nome__icontains',
            'luogo__icontains',
            'comune__icontains',
            'data_inizio__icontains',
            'data_fine__icontains',
            'categorie__nome__icontains'
        ]
        return handleList(Evento, request, filter, 'file', '-data_inizio', 'e')
    
    @method_decorator(permission_required('ph.add_evento', raise_exception=True))
    def post(self, request):
        return handleCreate(EventoForm, request, 'Evento', 'nome_f', 'nome', {'user': request.user.id})


class EventoView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_evento', raise_exception=True))
    def get(self, request, id):
        return handleGet(Evento, request, id, 'file', 'e')
    
    @method_decorator(permission_required('ph.change_evento', raise_exception=True))
    def post(self, request, id):
        return handleEdit(Evento, EventoForm, request, id, 'Evento', 'nome_f', {'user': request.user.id}, 'e')


class UtentiView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_user', raise_exception=True))
    def get(self, request):
        filter = [
            'username__icontains',
            'first_name__icontains',
            'last_name__icontains',
            'email__icontains'
        ]
        return handleList(User, request, filter, 'famiglia', '-date_joined')
    
    @method_decorator(permission_required('ph.add_user', raise_exception=True))
    def post(self, request):
        return handleCreate(UserCreationForm, request, 'User', 'username', 'username', {})


class UtenteView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_user', raise_exception=True))
    def get(self, request, id):
        return handleGet(User, request, id, 'famiglia')
    
    @method_decorator(permission_required('ph.change_user', raise_exception=True))
    def post(self, request, id):
        return handleEdit(User, UserChangeForm, request, id, 'User', 'username', {})


class ParoleView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_keyword', raise_exception=True))
    def get(self, request):
        return handleList(Keyword, request, 'nome__icontains', 'file', 'nome', 'p')
    
    @method_decorator(permission_required('ph.add_keyword', raise_exception=True))
    def post(self, request):
        return handleCreate(KeywordForm, request, 'Keyword', 'nome', 'nome', {})


class ParolaView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_keyword', raise_exception=True))
    def get(self, request, id):
        return handleGet(Keyword, request, id, 'file', 'p')
    
    @method_decorator(permission_required('ph.change_keyword', raise_exception=True))
    def post(self, request, id):
        return handleEdit(Keyword, KeywordForm, request, id, 'Keyword', 'nome', {}, 'p')


class AutoriView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_autore', raise_exception=True))
    def get(self, request):
        return handleList(Autore, request, 'nome__icontains', 'file', 'nome', 'a')
    
    @method_decorator(permission_required('ph.add_autore', raise_exception=True))
    def post(self, request):
        return handleCreate(AutoreForm, request, 'Autore', 'nome', 'nome')


class AutoreView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_autore', raise_exception=True))
    def get(self, request, id):
        return handleGet(Autore, request, id, 'file', 'a')
    
    @method_decorator(permission_required('ph.change_autore', raise_exception=True))
    def post(self, request, id):
        return handleEdit(Autore, AutoreForm, request, id, 'Autore', 'nome', {}, 'a')


class CategorieView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_categoria', raise_exception=True))
    def get(self, request):
        return handleList(Categoria, request, 'nome__icontains', 'evento__file', 'nome', 'c', True)
    
    @method_decorator(permission_required('ph.add_categoria', raise_exception=True))
    def post(self, request):
        return handleCreate(CategoriaForm, request, 'Categoria', 'nome', 'nome', {})


class CategoriaView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_categoria', raise_exception=True))
    def get(self, request, id):
        return handleGet(Categoria, request, id, 'evento__file')
    
    @method_decorator(permission_required('ph.change_categoria', raise_exception=True))
    def post(self, request, id):
        return handleEdit(Categoria, CategoriaForm, request, id, 'Categoria', 'nome', {})


class FamiglieView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_famiglia', raise_exception=True))
    def get(self, request):
        return handleList(Famiglia, request, 'nome__icontains', 'evento__file', 'nome')
    
    @method_decorator(permission_required('ph.add_famiglia', raise_exception=True))
    def post(self, request):
        return handleCreate(FamigliaForm, request, 'Famiglia', 'nome', 'nome', {})


class FamigliaView(LoginRequiredMixin, View):
    @method_decorator(permission_required('ph.view_famiglia', raise_exception=True))
    def get(self, request, id):
        return handleGet(Famiglia, request, id, 'evento__file')
    
    @method_decorator(permission_required('ph.change_famiglia', raise_exception=True))
    def post(self, request, id):
        return handleEdit(Famiglia, FamigliaForm, request, id, 'Famiglia', 'nome', {})
