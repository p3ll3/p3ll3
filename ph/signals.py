from django.db.models import signals
from django.dispatch import receiver
from .models import Evento, File
import logging

logger = logging.getLogger(__name__)


@receiver(signals.pre_save, sender=Evento)
def update_folder(sender, instance, **kwargs):
    create = True if "create" in kwargs and kwargs["created"] else False
    
    instance.set_folder(create)


@receiver(signals.post_delete, sender=Evento)
def remove_folder(sender, instance, **kwargs):
    instance.del_folder()


@receiver(signals.pre_delete, sender=File)
def remove_file(sender, instance, **kwargs):
    instance.del_file()


@receiver(signals.post_save, sender=File)
def extract_meta(sender, instance, **kwargs):
    logger.info("Upload File: (" + str(instance.id) + ") " + instance.file.name)
    instance.set_meta()
