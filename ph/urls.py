from django.urls import path

from . import views
from .views import IndexView, LoginView, LogoutView, SearchView, FilesView, FileView, EventiView, EventoView, UtentiView, UtenteView, ParoleView, ParolaView, AutoriView, AutoreView, CategorieView, CategoriaView, FamiglieView, FamigliaView

app_name='ph'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),

    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('e/', IndexView.as_view(), {'v': 'e'}),
    path('e/<int:id>/', IndexView.as_view(), {'v': 'e'}),

    path('p/', IndexView.as_view(), {'v': 'p'}),
    path('p/<str:id>/', IndexView.as_view(), {'v': 'p'}),

    path('a/', IndexView.as_view(), {'v': 'a'}),
    path('a/<str:id>/', IndexView.as_view(), {'v': 'a'}),
    
    path('c/', IndexView.as_view(), {'v': 'c'}),
    path('c/<str:id>/', IndexView.as_view(), {'v': 'c'}),
    
    
    path('search/', SearchView.as_view(), name='search'),
    
    path('files/', FilesView.as_view(), name='files'),
    path('files/<int:id>', FileView.as_view(), name='file'),

    path('eventi/', EventiView.as_view(), name='eventi'),
    path('eventi/<int:id>', EventoView.as_view(), name='evento'),

    path('parole/', ParoleView.as_view(), name='parole'),
    path('parole/<int:id>', ParolaView.as_view(), name='parola'),

    path('autori/', AutoriView.as_view(), name='autori'),
    path('autori/<int:id>', AutoreView.as_view(), name='autore'),
    
    path('categorie/', CategorieView.as_view(), name='categorie'),
    path('categorie/<int:id>', CategoriaView.as_view(), name='categorie'),

    path('utenti/', UtentiView.as_view(), name='utenti'),
    path('utenti/<int:id>', UtenteView.as_view(), name='utente'),
    
    path('famiglie/', FamiglieView.as_view(), name='famiglie'),
    path('famiglie/<int:id>', FamigliaView.as_view(), name='famiglie'),
]
