/**
* p3ll3
* Copyright (C) 2020  Andrea Pellegrini, <pelle@andreapellegrini.net>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var obj = {};
var notify;
var url_args;

$(function() {
  url_args = get_get();
  
  notify = new Notification();
  
  obj.grid = new Grid($("#file-grid"), "/files/", $(".cerca-grid"), "/search/");
  obj.slideshow = new Slideshow($("#slideshow"), "/files/", obj.grid.grid);
  
  obj.eventi = new CPList($("#cp-eventi"), "/eventi/");
  obj.eventi_form = new CPListFormEvento($("#cp-eventi"), "/eventi/", obj.eventi);
  
  obj.parole = new CPList($("#cp-parole"), "/parole/");
  obj.parole_form = new CPListForm($("#cp-parole"), "/parole/", obj.parole);
  
  obj.autori = new CPList($("#cp-autori"), "/autori/");
  obj.autori_form = new CPListForm($("#cp-autori"), "/autori/", obj.autori);
  
  obj.categorie = new CPList($("#cp-categorie"), "/categorie/");
  obj.categorie_form = new CPListFormUpdateSelect($("#cp-categorie"), "/categorie/", obj.categorie, $(".cp-field.categorie_select select"));
  
  obj.utenti = new CPList($("#cp-utenti"), "/utenti/");
  obj.utenti_form = new CPListFormUpdateSelect($("#cp-utenti"), "/utenti/", obj.utenti, $(".cp-field.utenti_select select"));
  
  obj.famiglie = new CPList($("#cp-famiglie"), "/famiglie/");
  obj.famiglie_form = new CPListFormUpdateSelect($("#cp-famiglie"), "/famiglie/", obj.famiglie, $(".cp-field.famiglie_select select"));
  
  
  obj.upload = new CPUpload("#cp-upload", "/files/", obj.grid);
  obj.upload_form = new CPUploadFormEvento($("#cp-upload"), "/eventi/", obj.upload);
  
  // obj.grid.load();// Load with popstate event triggering
  
  var wwidth = $(window).width();
  
  $(window).on("hashchange", function() {
    let hash = window.location.hash.substring(1);
    
    view_hash = hash;
    setState(window.location.origin + window.location.pathname + window.location.search + window.location.hash, false);
    
    if(hash) {
      if(obj.hasOwnProperty(hash)) {
        obj[hash].show();
      } else {
        hash = hash.split("-", 2);
        
        if(hash[0] == "file") {
          obj.slideshow.load(hash[1]);
        }
      }
    } else {
      let hashel = $(".hashel:visible").data("obj");
      
      if(hashel) {
        obj[hashel].hide(false);
      }
    }
  }).trigger("hashchange");
  
  $(window).on("popstate", function(e) {
    let state = history.state;
    
    if(state) {
      view_type = state["view_type"];
      view_id = state["view_id"];
      view_nome = state["view_nome"];
      
      obj.grid.push_state = false;
      obj.grid.load(state["args"]);
    }
  }).trigger("popstate");
  
  $(document).on("keydown", function(e) {
    let action = e.keyCode;

    if($("#slideshow").is(":visible")) {
      if(action == 27) {
        obj.slideshow.hide();
      } else if(action == 37) {
        obj.slideshow.prev();
      } else if(action == 39) {
        obj.slideshow.next();
      }
    } else if($(".cp").is(":visible") && action == 27) {
      obj[$(".cp:visible").data("obj")].hide();
    }
  });

  $("#navbar .select-view").on("click", function(e) {
    e.preventDefault();
     
    view_type = $(this).data("action");
    view_id = "";
    view_nome = "";
    
    obj.grid.load({});
  });
  
  $(".cerca-form .cerca-input, .cerca-form .cerca-button")
  .on("focus", function() {
    $(this).parent().addClass("active");
  })
  .on("blur", function() {
    $(this).parent().removeClass("active");
  });
  
  $(".cerca-form .cerca-input").on("keyup, submit", function() {
    var item = $(this).siblings(".reset-button");
    
    if($(this).val() != "") {
      $(item).addClass("active");
    } else {
      $(item).removeClass("active");
    }
  });
  
  
  $("#navbar .sandwich").on("click", function() {
    $(this).toggleClass("active");
    $("#navbar .nav-block-menu").toggleClass("active");
    $("body").toggleClass("fixed");
    console.log("ciao")
  });
  
  $("#navbar .nav-menu-title, .nav-menu-mobile-title").on("click", function(e) {
    e.stopPropagation();
    $(this).parent().toggleClass("active");
  });
  
  $("#navbar .nav-item").on("click", function() {
    $("#navbar .nav-item-list").removeClass("active");
    $("#navbar .sandwich").removeClass("active");
    $("#navbar .nav-block-menu").removeClass("active");
    $("body").removeClass("fixed");
  });
  
  $(window).on("resize", function() {
    let w = $(window).width()/wwidth;
    
    if(w <= 0.65 || w >= 1.55) {
      if(this.resizeTO) {
        clearTimeout(this.resizeTO);
      }
      
      this.resizeTO = setTimeout(function() {
        wwidth = $(window).width();
        obj.grid.build(true);
      }, 500);
    }
  });
  
});