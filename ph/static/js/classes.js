/**
* p3ll3
* Copyright (C) 2020  Andrea Pellegrini, <pelle@andreapellegrini.net>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function get_get() {
  let args = {};
  
  if(window.location.search) {
    let params = window.location.search.substr(1).split("&");
    
    for(let p of params) {
      p = p.split("=");
      args[p[0]] = decodeURIComponent(p[1]);
    }
  }
  
  return(args);
}

function setState(url, mode = true, args = url_args) {
  mode = mode ? "pushState" : "replaceState";
  
  window.history[mode](
    {
      "view_type": view_type,
      "view_id": view_id,
      "view_nome": view_nome,
      "view_hash": view_hash,
      "args": args
    },
    "",
    url
  );
}

function download(id, view = null, action = "") {
    let args = {};
    
    if(view) {
      args["z"] = id;
      args["v"] = view;
      id = 0;
    }
    
    if(action == "web") {
      args["w"] = "lveit";
    }
    
    if(Object.keys(args).length) {
      id += "?" + $.param(args);
    }
    
    window.location = "/files/" + id;
  }


class Notification {
  constructor() {
    if(url_args["msg"] == -2) {
      this.show("Non Autenticato", -2);
    }
  }
  
  show(data, mode = 0, duration = -1, color = "#000000") {
    var self = this;

    if(mode > 0) {
      color = "#66cc66";
    } else if(mode < 0) {
      duration = 4000;
      color = "#ff6060";
    }
    
    if(duration == -1) {
       duration = 2000;
    }

    var item = this.build(data, color, mode);
    $("#notification").append(item);

    $(item).fadeIn(function() {
      if(duration != 0) {
        setTimeout(self.hide.bind(self, item), duration);
      }
    })
    
    return(item);
  }

  build(data, color, mode) {
    var item = $("#notification-empty .notification-box").clone();
    $(item).find(".notification-text").text(data);
    $(item).css("background-color", color + "96").data("mode", mode); //Set opacity at 60% (Hex 128)
    return(item);
  }

  hide(item) {
    $(item).hide(300, function() {
      $(item).remove();
    });
  }
}

class Form {
  constructor(url) {
    this.url = url;
  }

  static toggle_form(item, button) {
    if($(item).hasClass("disabled")) {
      $(item).find(":input, button").prop("disabled", false);
    } else {
      $(item).find(":input, button").prop("disabled", true);
    }

    $(item).toggleClass("disabled");
    $(button).children().toggleClass("active").filter(".action-button-animation").toggleClass("loooader");
  }

  static print_errors(form, fields, errors) {
    for(const f of fields) {
      var item = $(form).find(":input[name=" + f.name + "]")[0];
      
      if(f.name in errors) {
        item.setCustomValidity(errors[f.name][0]["message"]);
      } else {
        item.setCustomValidity('');
      }
      
      item.reportValidity();
      item.setCustomValidity('');
    }
  }
  
  static failCallback(jqXHR, textStatus, errorThrown) {
    if(jqXHR.status == 401) {
      notify.show(jqXHR.responseJSON.error, -2, 0);
      window.location = "/login/?next=" + encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + "&msg=-2";
    } else {
      console.group("ERROR: AJAX");
      console.error(jqXHR);
      console.error(textStatus);
      console.error(errorThrown);
      console.trace(this);
      console.groupEnd();

      notify.show("Errore Script", -2, 0);
    }
  }
}

class Auth extends Form {
  constructor(form, url) {
    super(url)

    this.form = form;

    this.events();
  }

  events() {
    var self = this;

    $(this.form).find("form").on("submit", function() {
      self.login(this);
    });
  }

  login(el) {
    var form = $(el).serialize();
    var item = $(el).parents(".auth");
    var button = $(item).find(".submit-button");
    var error = $(this.form).find(".auth-field.error");
    var self = this;

    Form.toggle_form(item, button);

    $.ajax({url: this.url, method: "POST", data: form, dataType: "json"})
    .done(function(data) {
      if(data.return == 1) {
        notify.show("Autenticato", 1);
        $(error).fadeOut();
        self.redirect();
      } else if(data.return == -1) {
        notify.show("Errore Dati", -1);
        $(error).fadeIn();
      }
    })
    .fail(Form.failCallback)
    .always(function() {
      Form.toggle_form(item, button);
    });
  }
  
  redirect() {
    window.location = (url_args["next"] ? url_args["next"]: "/");
  }
}

class Grid {
  constructor(grid, url, cerca, cerca_url) {
    this.grid = grid;
    this.cerca = cerca;
    this.url = url;
    this.cerca_url = cerca_url;
    this.args = Object.assign({}, url_args);
    this.start;
    this.limit;
    this.tsize = "x200";
    this.count = [];
    this.count_r = 0;
    this.count_i = 0;
    this.offset = 50;
    this.more = 0;
    this.push_state = true;
    this.data = [];
    
    this.model_row = $("#file-grid-empty .file-grid-row");
    this.model_img = $("#file-grid-empty .file-grid-img");
    this.model_evento = $("#file-grid-empty .file-grid-evento");
    this.model_loader = $("#file-grid-empty .file-grid-loader");
    this.model_noresult = $("#file-grid-empty .file-grid-noresult");
    this.model_more = $("#file-grid-empty .file-grid-more");
    
    this.model_cerca = $(this.cerca).find(".cerca-empty .cerca-list-item");
    this.cerca_list = $(this.cerca).find(".cerca-list");
    this.model_filter = $("#filter-empty .filter-box");
    this.filter_block = $(".filter-block");
    
    if(view_msg == -1) {
      notify.show("Non Trovato", -1, 2000);
    }
    
    this.events();
  }

  events() {
    var self = this;

    $(this.grid).on("updated", function() {
      self.load({});
    });

    $(this.cerca).find("form").on("submit", function(e) {
      e.stopPropagation();
      view_type = "";
      view_id = "";
      view_nome = "";
      self.load({"q": $(this).find(".cerca-input").val()});
    });
    
    $(this.cerca).find("form").on("reset", function() {
      view_type = "";
      view_id = "";
      view_nome = "";
      $(this).find(".cerca-input").val("");
      $(this).parent(".filter-box").remove();
      $(this).submit();
    });

    $(this.filter_block).on("click", ".filter-box .remove", function() {
      view_type = "";
      view_id = "";
      view_nome = "";
      $(this).parent(".filter-box").remove();
      self.load({"q": ''});
    });

    $(this.grid).on("click", ".file-grid-evento", function(e) {
      if($(e.target).hasClass("dl")) {
        return;
      }
      
      view_type = $(this).data("view");
      view_id = $(this).data("id");
      view_nome = $(this).data("nome");
      self.load({});
    });
    
    $(this.grid).on("click", ".file-grid-item .download-button", function(e) {
      e.preventDefault();
      $(e.target).parent().toggleClass("active");
    });
    
    $(this.grid).on("click", ".file-grid-item .download", function(e) {
      e.preventDefault();
      let item = $(this).parents(".file-grid-item");
      download($(item).data("id"), $(item).data("view"), $(e.target).data("action"));
    });
    
    $(this.grid).on("click", ".file-grid-more", function() {
      $(this).children(".more").addClass("loooader").text("");
      $(this).children(".count").remove();
      self.load(self.args, $(this).data("end"));
    });
    
    $(this.cerca_list).on("click", ".cerca-list-item", function() {
      view_type = $(this).data("view");
      view_id = $(this).data("id");
      view_nome = $(this).data("nome");
      self.load({});
    });
    
    $(this.cerca).find(".cerca-input").on("keyup", function(e) {
      e.stopPropagation();
      let query = $(this).val()
      
      if(query && e.keyCode != 13) {
        self.search(query);
        $(self.cerca_list).addClass("active");
      } else {
        $(self.cerca_list).removeClass("active");
        if(!query){
          $(self.cerca_list).find(".cerca-list-item").remove();
        }
      }
    });
    
    $(this.cerca).find(".cerca-input").on("focusout", function(e) {
      e.stopPropagation();
      $(self.cerca_list).removeClass("active");
    });
    
    $(this.cerca).find(".cerca-input").on("focusin", function(e) {
      e.stopPropagation();
      
      if($(self.cerca_list).children().length && $(this).val()) {
        $(self.cerca_list).addClass("active");
      }
    });
  }

  load(a = this.args, s = 0, l = s + this.offset) {
    let loading = notify.show("Loading...");

    this.args = Object.assign({}, a); //Store args in query (default: args)
    this.start = parseInt(s); //Store s in start (default: 0)
    this.limit = parseInt(l); //Store l in limit (default: 50)

    let url_path = ""; //Store url path
    let url_params = ""; //Store url parameters
    
    for(const k in a) {
      if(!a[k].length) {
        delete a[k];
      }
    }

    $(this.filter_block).find(".filter-box").remove();
    
    if(view_type) {
      a["v"] = view_type;
      url_path = view_type + "/";

      if(view_id) {
        a[view_type] = view_id;
        url_path += view_id + "/";

        this.add_filter.bind(this)([view_type, view_id, view_nome]);
      }
    } else if(view_id) {
      a["q"] = view_id;
      this.add_filter.bind(this)([view_type, view_id, view_nome]);
    } else if(a["q"]) {
      view_id = a["q"];
      view_nome = a["q"];
      this.add_filter.bind(this)([view_type, view_id, view_nome]);
    } else {
      view_type = "";
      view_id = "";
      view_nome = "";
    }
    
    $("#navbar .nav-item.view-" + view_type).addClass("active").siblings(".nav-item").removeClass("active");
    
    $(this.cerca).find(".cerca-view").children().className == "view-" + view_type;
    $(this.cerca).find(".cerca-input").val(a["q"]);
    $(this.cerca_list).removeClass("active");
    
    if(a["q"]) {
      $(this.cerca).find(".reset-button").addClass("active");
    } else {
      $(this.cerca).find(".reset-button").removeClass("active");
    }
    
    if(Object.keys(a).length) {
      url_params += "?" + $.param(a);
      $(this.cerca).find(".cerca-input").val(a["q"]);
    }
    
    if(this.push_state) {
      setState(window.location.origin + "/" + url_path + url_params + window.location.hash, true, this.args);
    } else {
      this.push_state = true;
    }

    a["s"] = s;
    a["l"] = l;
    a["t"] = this.tsize;

    if(!this.start) { //Check if query start from 0
      $(this.grid).find(".file-grid-row").remove(); //Clean grid
      this.data = [];
      this.count_r = 0; //Reset row counter
      this.count_i = 0; //Reset item counter
      $(this.grid).append($(this.model_loader).clone());
    }

    $.get(this.url, a, this.build.bind(this, false), "json").fail(Form.failCallback).always(notify.hide(loading));
  }

  add_filter(f) {
    let item = $(this.model_filter).clone().data("filter", f[0]).addClass("filter-" + f[0]);
    $(item).find(".nome").text(f[2]);
    $(this.filter_block).append(item);
  }

  build(rebuild, data = []) {
    
    if(rebuild) {
      data = this.data;
      this.count_i = 0;
      $(this.grid).find(".file-grid-row").remove();
    } else {
      this.count = data.splice(0, 1)[0];
      this.data.push(...data);
    }
    
    if (!this.count["count"]) {
      $(this.grid).find(".file-grid-loader").remove();
      $(this.grid).append(this.new_row(0).append($(this.model_noresult).clone()));
      return;
    }
    
    const max_w = $(this.grid).width(); //max_w: Grid width
    const row_ext = max_w > 800 ? 20: max_w > 400 ? 50 : 70;
    const max_w_ext = (max_w + (max_w/100*row_ext));
    let row_w = 0; //row_w: Store current row width
    
    if(this.count_i == 0) {
      this.count_r = 1;
      $(this.grid).find(".file-grid-loader").remove();
      $(this.grid).append(this.new_row(1)); //Append first row to grid
    } else {
      let morel = $(this.grid).find(".file-grid-more");
      row_w = $(morel).data("row_w");
      $(morel).remove();
    }
    
    for(const d of data) {
      row_w += d.twidth; //Add current item width to row_w
      this.count_i += 1; //Update item count
      
      if(row_w >= max_w && row_w >= max_w_ext) { //Check if row_w is less or equal then max_w and max_w_ext
        this.count_r += 1;
        row_w = d.twidth; //Reset current row_w with current item width
        $(this.grid).append(this.new_row.bind(this)(this.count_r)); //Create new row and append to grid
      }
      
      $(this.grid).find(".file-grid-row#row-" + this.count_r).append(this.new_item.bind(this)(this.count_i, d)); //Create new item and append to row
    }
    
    if(this.count["more"] > 0) {
      $(this.grid).find(".file-grid-row#row-" + this.count_r).append(this.add_more(this.count, row_w));
    }
  }

  new_item(i, d) {
    if(d.cover) {
      var item = $(this.model_evento).clone();
      var nome = d.nome_f;

      $(item).find(".data").text(d.data);
      $(item).find(".count").text(d.count);
      $(item).find(".title").text(d.nome);

      var j = 0;
      for(const t in d.cover) {
        $(item).find(".container img").eq(j).attr("src", "/files/" + d.cover[t] + "?t=cover");
        j += 1;
      }

      $(item).data("view", d.view)
    } else {
      var item = $(this.model_img).clone().attr("href", "#file-" + d.id).addClass("item-" + i + " file-" + d.id);
      var nome = d.file;

      $(item).find(".data").text(d.data);
      $(item).find(".title").text(d.keywords_f);

      if(d.twidth) {
        $(item).find("img").attr("src", "/files/" + d.id + "?t=x200");
        var j = 1;
      } else {
        var j = 0;
      }
    }

    if(j > 0) {
      $(item).find("img").eq(j-1).one("load", this.removeLoader);
    } else {
      $(item).addClass("empty");
      this.removeLoader.bind($(item).find("img").eq(0))()
    }

    $(item).data("id", d.id).data("i", i).data("nome", nome);

    return(item);
  }

  new_row(i) {
    return($(this.model_row).clone().attr("id", "row-" + i));
  }

  removeLoader() {
    $(this).siblings(".loooader").remove();
  }

  add_more(count, row_w) {
    this.more = count.more;
    var item = $(this.model_more).clone();
    $(item).data("end", this.limit).data("row_w", row_w);
    $(item).children(".count").text(count.limit + "/" + count.count);
    return(item);
  }
  
  search(query) {
    var self = this;
    
    $.get(this.cerca_url, {'v': view_type, 'id': view_id, 'q': query}, function(data) {
      $(self.cerca_list).find(".cerca-list-item").remove();
      
      if(data) {
        $(self.cerca_list).addClass("active");
      } else {
        $(self.cerca_list).removeClass("active");
      }
      
      for(const d of data) {
        let item = $(self.model_cerca).clone();
        
        $(item).addClass("view-" + d.view).data("view", d.view).data("id", d.id).data("nome", d.nome_f);
        $(item).find(".nome").text(d.nome_f);
        $(item).find(".count").text(d.count);
        
        $(self.cerca_list).append(item);
      }
    }, "json").fail(Form.failCallback);
  }
}

class Slideshow {
  constructor(slideshow, url, grid) {
    this.slideshow = slideshow;
    this.url = url;
    this.grid = grid;
    this.delay = 200;
    this.id;
    this.index;

    this.events();
  }

  events() {
    var self = this;

    $(this.slideshow).find(".control").on("click", function() {
      self[$(this).data("action")]();
    });

    $(this.slideshow).find(".field.keywords-list").on("click", ".keywords-item", function(e) {
      e.preventDefault();
      self.gridLoad(this);
    });
    
    $(this.slideshow).find(".field.autore, .field.evento").on("click", function(e) {
      e.preventDefault();
      self.gridLoad(this);
    });
    
    $(this.slideshow).find(".field.download .download-button").on("click", function(e) {
      e.preventDefault();
      $(e.target).siblings(".download-list").toggle();
    });
    
    $(this.slideshow).find(".field.download .download").on("click", function(e) {
      e.preventDefault();
      let item = $(this).parents(".field.download");
      download($(item).data("id"), null, $(e.target).data("action"));
    });
    
    $(this.slideshow).find(".field.image").on("load", this.hideLoader.bind(this));
    
    $(this.slideshow).find(".slideshow-image").on("click", function () {
      $(self.slideshow).toggleClass("nocontrols");
    });
  }

  get(el) {
    if($(el).length) {
      window.location.hash = "#file-" + $(el).data("id");
    } else {
      console.log("Elemento non trovato");
      this.hide();
    }
  }

  load(id, t = "x1000") {
    this.show();
    this.showLoader();
    this.id = id;
    self = this;
    
    setTimeout(function() {
      self.clean();
      $.get(self.url + id, {"s": "" }, self.build.bind(self, t), "json").fail(Form.failCallback);
    }, this.delay);
    
  }

  build(t, data) {
    $(this.slideshow).find(".field.image").attr("src", "/files/" + data.id + "?t=" + t);
    $(this.slideshow).find(".field.download").data("id", data.id);
    $(this.slideshow).find(".field.evento .data").text(data.evento.data);
    $(this.slideshow).find(".field.evento .nome").text(data.evento.nome);
    $(this.slideshow).find(".field.evento .luogo").text(data.evento.luogo_f);
    $(this.slideshow).find(".field.evento").data("id", data.evento.id).data("nome", data.evento.data + ": " + data.evento.nome).attr("href", "/e/" + data.evento.id);
    $(this.slideshow).find(".field.autore").data("id", data.autore.id).data("nome", data.autore.nome).attr("href", "/a/" + data.autore.id).text(data.autore.nome);
    
    let k_empty = $(this.slideshow).find(".field-empty .keywords-item");
    let k_list = []
    
    for(const k in data.keywords) {
      k_list.push($(k_empty).clone().text(data.keywords[k]).data("id", k).data("nome", data.keywords[k]).attr("href", "/p/" + k));
    }
    
    let m_list = $(this.slideshow).find(".field.meta-list");
    
    for(const m of data.meta.meta_list) {
      $(m_list).find("." + m).text(data.meta[m]);
    }
    
    $(this.slideshow).find(".field.keywords-list").append(k_list);
  }

  show() {
    $(this.slideshow).fadeIn(300, function() {
      $("body").addClass("fixed");
    });
    
  }

  hide(rmhash = true) {
    if(rmhash) {
      this.removeHash();
      return;
    }
    
    $("body").removeClass("fixed");
    this.scrollToItem(this.id);
    $(self.slideshow).removeClass("nocontrols");
    
    $(this.slideshow).fadeOut(300, this.clean.bind(this));
  }

  removeHash() {
    view_hash = "";
    window.location.hash = "";
  }

  clean() {
    $(this.slideshow).find(".field.image").attr("src", "");
    $(this.slideshow).find(".field.download").removeData("id");
    $(this.slideshow).find(".field.evento .evento-item").text("");
    $(this.slideshow).find(".field.evento").removeData("id");
    $(this.slideshow).find(".field.autore").removeData("id").text("");
    $(this.slideshow).find(".field.meta .meta-item").text("");
    $(this.slideshow).find(".field.keywords .keywords-item").remove();
  }

  prev() {
    this.index = $(this.grid).find(".file-grid-item.file-" + this.id).data("i");
    this.get($(this.grid).find(".file-grid-item.item-" + (this.index-1)));
  }

  next() {
    this.index = $(this.grid).find(".file-grid-item.file-" + this.id).data("i");
    this.get($(this.grid).find(".file-grid-item.item-" + (this.index+1)));
  }

  showLoader() {
    $(this.slideshow).find(".slideshow-image .loooader").css("display", "block");
    $(this.slideshow).addClass("loading");
  }

  hideLoader() {
    $(this.slideshow).find(".slideshow-image .loooader").css("display", "none");
    $(this.slideshow).removeClass("loading");
  }
  
  scrollToItem(id) {
    let item = $(this.grid).find(".file-grid-item.file-" + id);
    
    if(item.length) {
      $(item)[0].scrollIntoView(false);
    }
  }
  
  gridLoad(el) {
    view_type = $(el).data("view");;
    view_id = $(el).data("id");
    view_nome = $(el).data("nome");
    $(this.grid).trigger("updated");
    this.hide();
  }
}

class CP {
  constructor(cp) {
    this.cp = cp;
    
    this.events();
  }

  events() {
    var self = this;

    $(this.cp).on("keydown", function(e) {
      if(e.keyCode == 27) {
        self.hide();
      }
    });

    $(this.cp).find(".cp-button").on("click", function() {
      let action = $(this).data("action");

      if(action == "hide") {
        self.hide();
      }
    });

    $(this.cp).on("click", function(e) {
      if($(e.target).hasClass("cp")) {
        self.hide();
      }
    });
  }

  show() {
    $("body").addClass("fixed");
    $(this.cp).fadeIn(300);
  }

  hide(rmhash = true) {
    if(rmhash) {
      this.removeHash();
      return;
    }
    
    $("body").removeClass("fixed");
    $(this.cp).fadeOut();
  }

  removeHash() {
    view_hash = "";
    window.location.hash = "";
  }

}

class CPList extends CP {
  constructor(cp, url) {
    super(cp);

    this.list = $(cp).find(".cp-list");
    this.url = url;
    this.query;
    this.start;
    this.limit;
    this.offset = 25;
    this.count;
    this.storage = {};
    
    this.model_edit = $(cp).find(".cp-block-empty .model-edit");
    this.model_create = $(cp).find(".cp-block-empty .model-create");
    this.model_row = $(cp).find(".cp-block-empty .cp-list-row");
    this.model_loader = $(cp).find(".cp-block-empty .cp-list-loader");
    this.model_more = $(cp).find(".cp-block-empty .cp-list-more");
    this.model_noresult = $(cp).find(".cp-block-empty .cp-list-noresult");
    this.view_type = $(cp).data("view_type");
    this.view_nome = $(cp).data("view_nome");
    
    this.eventsCpList();
  }

  eventsCpList() {
    var self = this;

    $(this.cp).find(".cerca-cp form").on("submit", function(e) {
      e.stopPropagation();
      self.load($(this).find("input[name=q]").val());
    });

    $(this.cp).on("updated", ".cp-list-row", function() {
      self.load(this.query, this.start, this.limit);
    });

    $(this.cp).find(".cp-list").on("click", ".cp-list-action.filtra", function() {
      let row = $(this).parents(".cp-list-row");
      view_type = self.view_type;
      view_id = $(row).data("id");
      view_nome = $(row).data("nome");

      //DA SISTEMARE
      $("#file-grid").trigger("updated");

      self.hide();
    });
    
    $(this.cp).find(".cp-list").on("click", ".cp-list-action.download", function() {
      let id = $(this).parents(".cp-list-row").data("id");
      window.location = "/files/0?v=" + self.view_type + "&z=" + id;
    });
    
    $(this.cp).on("click", ".cp-list-more", function() {
      $(this).addClass("cp-list-loader");
      $(this).children(".more").addClass("loooader");
      self.load(self.args, $(this).data("end"));
    });
  }
  
  load(q = this.query, s = 0, l = s + this.offset) {
    this.query = q;
    this.start = s;
    this.limit = l;

    if(this.start == 0) {
      this.clean();
    }

    $(this.cp).find(".cerca-cp input[name=q]").val(q);
    
    $.get(this.url, {"q": q, "s": s, "l": l}, this.build.bind(this), "json").fail(Form.failCallback);
  }

  build(data) {
    let count = data.splice(0, 1)[0];

    if (!count.count) {
      $(this.list).find(".cp-list-loader").remove();
      $(this.list).find(".cp-list-body").append($(this.model_noresult).clone());
      return;
    }

    if(this.count == 0) {
      $(this.list).find(".cp-list-loader").remove();
    } else {
      $(this.list).find(".cp-list-more").remove();
    }

    for(const d of data) {
      this.storage[d.id] = d;
      this.count += 1;

      let row = $(this.model_row).clone();
      $(row).data("id", d.id).data("nome", d.nome_f);

      for(const l of d.list) {
        $(row).find("." + l).text(d[l]);
      }

      $(this.list).find(".cp-list-body").append(row);
    }
    
    if(count.more > 0) {
      $(this.list).find(".cp-list-body").append(this.add_more(count));
    }
    
    $(this.list).find(".cp-list-footer .count").text(this.count + "/" + count.count);
  }

  clean() {
    $(this.list).find(".cp-list-body").children().remove();
    $(this.list).find(".cp-list-body").append($(this.model_loader).clone());
    this.count = 0;
  }

  show() {
    this.load();
    super.show();
  }

  add_more(count) {
    this.more = count.more;
    var item = $(this.model_more).clone();
    $(item).data("end", this.limit);
    return(item);
  }
}

class CPForm extends Form {
  constructor(cp, url) {
    super(url);

    this.cp = cp;

    this.events();
  }

  events() {
    var self = this;

    $(this.cp).on("submit", ".cp-form form.save", function() {
      self.saveForm($(this));
    });

    $(this.cp).on("click", ".cp-form .delete-button", function() {
      if($(this).attr("action") == "cancel") {
        self.toggle_edit($(this), true);
      } else {
        self.deleteForm($(this));
      }
    });
  }

  saveForm(el) {
    if(!$(el).find(".cp-field.data_multi input").is(":checked")) {
      var form = $(el).find(":input").not(".cp-field.data_fine select").serializeArray();
    } else {
      var form = $(el).serializeArray();
    }

    var item = $(el).parent(".cp-form");
    var id = $(item).data("id");
    id = (id ? id: "");
    var button = $(item).find(".submit-button");
    var cp = $(item).parents(".cp").attr("id");
    var self = this;

    Form.toggle_form(item, button);

    $.ajax({
      url: this.url + id,
      method: "POST",
      data: form,
      dataType: "json"
    })
    .always(function() {
      Form.toggle_form(item, button);
    })
    .done(function(data) {
      Form.print_errors(item, form, data.errors);
      
      if(data.return == 1) {
        notify.show("Salvato", 1);
        self.saveCallback(item, data);
      } else if(data.return == -1) {
        notify.show("Errore Campi", -1);
      }
    })
    .fail(Form.failCallback);
  }

  deleteForm(el) {
    var item = $(el).parents(".cp-form");
    var button = $(el);
    var id = $(item).data("id");
    var csrf = $(item).find("[name=csrfmiddlewaretoken]").val();
    var self = this;

    Form.toggle_form(item, button);

    $.ajax({
      url: this.url + id,
      method: "POST",
      data: {
        "csrfmiddlewaretoken": csrf,
        "delete": true
      },
      dataType: "json"
    })
    .always(function() {
      Form.toggle_form(item, button);
    })
    .done(function(data) {
      if(data.return == 1) {
        notify.show("Eliminato", 1);
        self.deleteCallback(item, data);
      } else {
        notify.show("Non Eliminato", -1);
      }
    })
    .fail(Form.failCallback);
  }

  saveCallback(item, data) {
    console.log("Salvato! (sono la callback)");
  }

  deleteCallback(item, data) {
    console.log("Rimosso! (sono la callback)");
  }
}

class CPListForm extends CPForm {
  constructor(cp, url, cpobj) {
    super(cp, url);

    this.cpobj = cpobj;
    this.list = cpobj.list;
    this.model_edit = cpobj.model_edit;
    this.model_create = cpobj.model_create;
    this.storage = cpobj.storage;

    this.eventsCpListForm();
  }

  eventsCpListForm() {
    var self = this;

    $(this.list).on("click", ".cp-list-action.edit", function() {
      self.toggle_edit(this);
    });

    $(this.list).on("click", ".cp-list-action.create", function() {
      let el = $(self.cp).find(".cp-block-empty .cp-list-create").clone().addClass("cp-list-row");
      $(self.list).find(".cp-list-body").prepend(el);
      self.toggle_edit($(el).children(), true);
    });
  }

  toggle_edit(el, create = false) {
    $(el).toggleClass("active");
    var item = $(el).parents(".cp-list-row");

    if($(el).hasClass("active")) {
      if(create) {
        var row = $(this.model_create).clone();
        $(row).find(".action-button.delete-button").attr("action", "cancel").addClass("active").children(".action-button-text").text("Annulla");
      } else {
        var row = $(this.model_edit).clone();
        var data = this.storage[$(item).data("id")];
        row = this.toggleCallback(row, data);
      }
      
      $(item).append(row);
      $(item).find(".cp-list-edit-row").show(500);
    } else {
      self = this;
      
      $(item).find(".cp-list-edit-row").hide(500, function() {
        if(create) {
          if($(el).attr("action") == "cancel") {
            $(item).remove();
          } else {
            $(item).replaceWith($(self.model_loader).clone().addClass("cp-list-row"));
          }
        } else {
          $(this).remove();
        }
      });
    }
  }

  toggleCallback(row, data) {
    for(const e of data.edit) {
      $(row).find(".cp-field." + e + " :input").val(data[e]);
    }

    $(row).data("id", data.id);
    return(row);
  }

  saveCallback(item, data) {
    this.toggle_edit($(item).siblings(".controls").find(".edit"));
    $(item).parents(".cp-list-row").trigger("updated");
  }

  deleteCallback(item, data) {
    this.toggle_edit($(item).siblings(".controls").find(".edit"));
    $(item).parents(".cp-list-row").trigger("updated").remove();
  }
  
  updateSelect(select) {
    $.get(this.url, {}, function(data) {
      data.splice(0, 1);
      
      $(select).find("option").not("[value='']").remove();
      
      for(const d of Object.values(data)) {
        $(select).append("<option value=\"" + d.id + "\">" + d.nome_f + "</option>");
      }
    }, "json").fail(Form.failCallback);
  }
}

class CPUpload extends CP {
  constructor(cp, url, grid) {
    super(cp);

    this.url = url;
    this.close = true;
    this.grid = grid;
    
    this.cp_selector = $(cp).find(".cp-selector");
    this.form_evento = $(cp).find(".cp-form.form_evento");
    this.form_evento_select = $(cp).find(".cp-form.form_evento_select");
    this.form_file = $(cp).find(".cp-form.form_file");
    
    this.eventsCpUpload();
  }

  eventsCpUpload() {
    var self = this;

    $(this.cp_selector).find(".cp-selector-field").on("click", function() {
      self.select_mode(this);
    });

    $(this.form_evento_select).find("form").on("submit", function() {
      let el = $(this).find("select[name=evento]");
      let id = $(el).val();
      self.upload_select_evento(id, $(el).find("option[value=" + id + "]").text());
    });

    $(this.cp_selector).on("click", ".filter-box .remove", function() {
      self.clean();
    });
    
    $(this.form_file).find(".cp-field.file input").on("change", function() {
      if($(this).val()) {
        self.close = false;
      } else {
        self.close = true;
      }
    });
    
    $(this.form_file).find("form.upload").on("submit", function() {
      self.files();
    });
  }

  files() {
    var item = $(this.form_file);
    var formData = new FormData($(item).children("form")[0]);
    var probar = $(item).find(".progress-bar");
    var button = $(item).find(".submit-button");
    var self = this;

    Form.toggle_form(item, button);
    this.toggle_progress();

    $.ajax({
      xhr: function() {
        var xhr = new window.XMLHttpRequest();

        xhr.upload.addEventListener("progress", function(e) {
          if (e.lengthComputable) {
            var perc = parseInt(e.loaded / e.total * 100);
            $(probar).css("width", perc + "%").children(".progress-text").text(perc);
          }
        }, false);

        return xhr;
      },
      url: this.url,
      method: "POST",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json"
    })
    .always(function() {
      self.toggle_progress();
      Form.toggle_form(item, button);
    })
    .done(function(data) {
      if(data.return == 1) {
        notify.show("Files Caricati", 1);
        self.update_grid(self.grid, $(item).data("view_type"), $(item).data("view_id"), $(item).data("view_nome"));
        self.close = true;
        self.hide();
      } else {
        notify.show("Non Caricati", -1);
      }
    })
    .fail(Form.failCallback);
  }

  select_mode(el) {
    $(el).addClass("active").siblings().removeClass("active");
    $(this.cp).find(".cp-form").removeClass("active").find("form").trigger("reset");
    $(this.cp).find(".cp-form." + $(el).data("action")).addClass("active");
  }

  clean() {
    $(this.cp).find(".cp-form").removeClass("active");
    $(this.cp).find(".cp-selector .cp-selector-field").removeClass("active").show();
    $(this.cp).find(".cp-selector .filter-box").remove();
    $(this.cp).find(".cp-form form").trigger("reset");
    $(this.cp).find(".cp-form .cp-field.data_fine").removeClass("active");
  }
  
  show() {
    this.clean();
    super.show();
  }
  
  hide(rmhash = true) {
    if(rmhash) {
      this.removeHash();
      return;
    }
    
    if(!this.close) {
      if(!window.confirm("Stai caricando dei files. Sicuro di voler uscire?")) {
        return;
      }
    }
    
    $("body").removeClass("fixed");
    
    var self = this;
    $(this.cp).fadeOut(400, function() {
      self.clean();
      this.close = true;
    });
  }

  upload_select_evento(id, nome_f) {
    $(this.cp).find(".cp-form").removeClass("active");
    $(this.cp).find(".cp-form.form_file").addClass("active").data("view_type", "e").data("view_id", id).data("view_nome", nome_f);
    $(this.cp).find(".cp-form.form_file [name=evento]").val(id);

    let filter = $("#filter-empty .filter-box").clone();
    $(filter).find(".nome").text(nome_f);
    $(this.cp).find(".cp-selector .cp-selector-field").hide();
    $(this.cp).find(".cp-selector").append(filter);
  }

  toggle_progress() {
    $(this.form_file).find(".cp-field.progress").toggle();
  }
  
  update_grid(grid, view, id, nome) {
    view_type = view;
    view_id = id;
    view_nome = nome.replace(/ \([0-9]+\)$/, "");
    grid.load({});
    obj.upload_form.updateSelectEvento(obj.upload_form.select);
  }
}

const CPFormEvento = (cls) => class extends cls {
  eventsCPFormEvento() {
    var self = this;

    $(this.cp).on("change", ".cp-form .cp-field.data_multi input", function() {
      self.toggle_data_multi(this);
    });
  }

  toggle_data_multi(el) {
    var form = $(el).parents(".cp-form");
    var data_fine = $(form).find(".cp-field.data_fine");

    if($(el).is(":checked")) {
      $(data_fine).addClass("active");

      var data_inizio = $(form).find(".cp-field.data_inizio");

      $(data_fine).find("[name=data_fine_month]").val($(data_inizio).find("[name=data_inizio_month]").val());
      $(data_fine).find("[name=data_fine_day]").val($(data_inizio).find("[name=data_inizio_day]").val());
      $(data_fine).find("[name=data_fine_year]").val($(data_inizio).find("[name=data_inizio_year]").val());
    } else {
      $(data_fine).removeClass("active");
    }
  }

  toggleCallback(row, data) {
    var data_inizio = data["data_inizio"].split("-");
    $(row).find(".cp-field.data_inizio [name=data_inizio_year]").val(parseInt(data_inizio[0]));
    $(row).find(".cp-field.data_inizio [name=data_inizio_month]").val(parseInt(data_inizio[1]));
    $(row).find(".cp-field.data_inizio [name=data_inizio_day]").val(parseInt(data_inizio[2]));

    if(data["data_multi"]) {
      $(row).find(".cp-field.data_multi input").prop("checked", true);

      var data_fine = data["data_fine"].split("-");
      $(row).find(".cp-field.data_fine [name=data_fine_year]").val(parseInt(data_fine[0]));
      $(row).find(".cp-field.data_fine [name=data_fine_month]").val(parseInt(data_fine[1]));
      $(row).find(".cp-field.data_fine [name=data_fine_day]").val(parseInt(data_fine[2]));

      $(row).find(".cp-field.data_fine").addClass("active");
    } else {
      $(row).find(".cp-field.data_multi").prop("checked", false);
    }

    row = super.toggleCallback(row, data);

    return(row);
  }

  saveCallback(item, data) {
    let cp = $(this.cp).attr("id");
    if(cp == "cp-upload") {
      this.cpobj.upload_select_evento(data.id, data.nome_f);
    } else if(cp == "cp-eventi") {
      this.toggle_edit($(item).siblings(".controls").find(".edit"));
      $(item).parents(".cp-list-row").trigger("updated");
    }
    
    this.updateSelectEvento(this.select);
  }
  
  deleteCallback(item, data) {
    super.deleteCallback(item, data);
    this.updateSelectEvento(this.select);
  }
  
  updateSelectEvento(select) {
    $.get(this.url, {'select': 1}, function(data) {
      $(select).find("option, optgroup").not("option[value='']").remove();
      delete data[''];
      
      for(const [m, d] of Object.entries(data)) {
        var group = $("<optgroup label=\"" + m + "\"></optgroup>");
        
        for(const e of d) {
          $(group).append("<option value=\"" + e[0] + "\">" + e[1] + "</option>");
        }
        
        $(select).append(group);
      }
    }, "json").fail(Form.failCallback);
  }
}

class CPListFormEvento extends CPFormEvento(CPListForm) {
  constructor(cp, url, cpobj) {
    super(cp, url, cpobj);
    
    this.select = $(".cp-field.evento_select select");
    
    this.eventsCPFormEvento();
  }
}

class CPUploadFormEvento extends CPFormEvento(CPForm) {
  constructor(cp, url, cpobj) {
    super(cp, url);

    this.cpobj = cpobj;
    this.select = $(".cp-field.evento_select select");
    
    this.eventsCPFormEvento();
  }
}

class CPListFormUpdateSelect extends CPListForm {
  constructor(cp, url, cpobj, select) {
    super(cp, url, cpobj);
    
    this.select = select;
  }
  
  saveCallback(item, data) {
    super.saveCallback(item, data);
    this.updateSelect(this.select);
  }
  
  deleteCallback(item, data) {
    super.deleteCallback(item, data);
    this.updateSelect(this.select);
  }
}