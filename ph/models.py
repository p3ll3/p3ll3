from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.timezone import now
from django.utils.text import slugify

from sorl.thumbnail import ImageField, get_thumbnail
from PIL import Image
from io import BytesIO
from fractions import Fraction

from datetime import datetime
import os, logging
#from asgiref.sync import sync_to_async

logger = logging.getLogger(__name__)


def list_to_ne(items, field):
    d, f, l = {}, "", []
    
    if items:
        for i in items:
            d[i["id"]] = i[field]
            f += i[field] + ", "
            l.append(i["id"])
            
        f = f[:-2]
    
    return d, f, l


class Famiglia(models.Model):
    nome = models.CharField(max_length=100)
    users = models.ManyToManyField(User, blank=True)
    
    def __str__(self):
        return self.nome
    
    def as_dict(self):
        users_d, users_f, users_l = list_to_ne(self.users.all().values('id', 'username'), 'username')
        
        return {
            "id": self.id,
            "nome": self.nome,
            "nome_f": self.nome,
            "users_d": users_d,
            "users_f": users_f,
            "users": users_l,
            "count": self.count,
            "list": [
                "nome",
                "users_f",
                "count"
            ],
            "edit": [
                "nome",
                "users"
            ]
        }


class Categoria(models.Model):
    nome = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nome
    
    def as_dict(self, cover = True):
        return {
            "id": self.id,
            "view": "c",
            "nome": self.nome,
            "nome_f": self.nome,
            "cover": cover,
            "twidth": 200,
            "count": self.count,
            "list": [
                "nome",
                "count"
            ],
            "edit": [
                "nome"
            ]
        }


class Evento(models.Model):
    nome = models.CharField(max_length=100)
    data_inizio = models.DateField(null=True, default=now)
    data_multi = models.BooleanField(default=False)
    data_fine = models.DateField(null=True, blank=True)
    luogo = models.CharField(max_length=100, blank=True)
    comune = models.CharField(max_length=100, blank=True)
    descrizione = models.CharField(max_length=500, blank=True)
    categorie = models.ManyToManyField(Categoria, blank=True)
    utente = models.ForeignKey(User, models.SET_NULL, blank=True, null=True)
    folder = models.CharField(max_length=100, null=True)
    famiglie = models.ManyToManyField(Famiglia)
    

    def set_folder(self, create = False):
        path = os.path.join(self.data_inizio.strftime("%Y/%m/%d/"), slugify(self.nome, allow_unicode=True))
        
        if self.folder != path + "/":
            root = settings.MEDIA_ROOT
            new_path = path + "/"
            i = 0
            
            while os.path.exists(os.path.join(root, new_path)):
                i += 1
                new_path = path + "-" + str(i) + "/"
            
            logger.info("New Folder: " + new_path)
            os.makedirs(os.path.join(root, new_path))
            
            files = self.file_set.all()
            
            for f in files:
                name = os.path.join(new_path, os.path.basename(f.file.name))
                logger.info("Move File: " + f.file.path + " -> " + name)
                os.rename(f.file.path, os.path.join(root, name))
                f.file.name = name
                f.save()
            
            if not create and self.folder:
                logger.info("Remove Folder: " + self.folder)
                os.rmdir(os.path.join(root, self.folder))
            
            self.folder = new_path


    def del_folder(self):
#         files = self.file_set.all()
# 
#         for f in files:
#             logger.info("Remove File: " + f.file.path)
#             os.remove(f.file.path)
        if self.folder:
            logger.info("Remove Folder: " + self.folder)
            os.rmdir(os.path.join(settings.MEDIA_ROOT, self.folder))
    
    
    def __str__(self):
        return self.nome_f
    
    
    @property
    def nome_f(self):
        return self.data_f + ": " + self.nome
    
    
    @property
    def luogo_f(self):
        if self.comune:
            if self.luogo:
                return self.luogo + " [" + self.comune + "]"
            else:
                return self.comune
        else:
            return self.luogo


    @property
    def data_f(self):
        if self.data_multi:
            di = self.data_inizio.strftime("%Y-%m-%d").split("-")
            df = self.data_fine.strftime("%Y-%m-%d").split("-")
            ndi = ""
            ndf = "-"
            diff = False
            
            for i, f in zip(di, df):
                ndi += i + "/"
                if i != f or diff:
                    diff = True
                    ndf += f + "/"
        
            return ndi[:-1] + ndf[:-1]
        else:
            return self.data_inizio.strftime("%Y/%m/%d")


    def as_dict(self, cover = True):
        categorie_d, categorie_f, categorie_l = list_to_ne(self.categorie.all().values("id", "nome"), "nome")
        famiglie_d, famiglie_f, famiglie_l = list_to_ne(self.famiglie.all().values("id", "nome"), "nome")

        return {
            "id": self.id,
            "view": "e",
            "nome": self.nome,
            "data_inizio": self.data_inizio,
            "data_multi": self.data_multi,
            "data_fine": self.data_fine,
            "data": self.data_f,
            "luogo": self.luogo,
            "luogo_f": self.luogo_f,
            "comune": self.comune,
            "descrizione": self.descrizione,
            "categorie_d": categorie_d,
            "categorie_f": categorie_f,
            "categorie": categorie_l,
            "famiglie_d": famiglie_d,
            "famiglie_f": famiglie_f,
            "famiglie": famiglie_l,
            "nome_f": self.nome_f,
            "cover": cover,
            "twidth": 200,
            "folder": self.folder,
            "count": self.count,
            "list": [
                "nome",
                "data",
                "luogo_f",
                "categorie_f",
                "count"
            ],
            "edit": [
                "nome",
                "luogo",
                "comune",
                "categorie",
                "descrizione",
                "famiglie"
            ]
        }


def EventoSelect(user):
    from django.utils.dates import MONTHS
    
    qs = Evento.objects.filter(famiglie__users=user).order_by('data_inizio').distinct()
    qs = qs.annotate(count=models.Count('file', distinct=True))
    eventi = {'': "- Seleziona -"}
    m = None
    
    for e in qs:
        if e.data_inizio.month != m:
            m = e.data_inizio.month
            month = MONTHS[m] + " " + str(e.data_inizio.year)
            eventi[month] = []
        
        eventi[month].append((e.id, e.nome_f + " (" + str(e.count) + ")"))
    
    return eventi


class Autore(models.Model):
    nome = models.CharField(max_length=50)
    soprannome = models.CharField(max_length=50, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    
    def __str__(self):
        return self.nome
    
    
    def as_dict(self, cover = True):
        return {
            "id": self.id,
            "view": "a",
            "nome": self.nome,
            "nome_f": self.nome,
            "cover": cover,
            "twidth": 200,
            "count": self.count,
            "list": [
                "nome",
                "count"
            ],
            "edit": [
                "nome"
            ]
        }


class Keyword(models.Model):
    nome = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    
    def __str__(self):
        return self.nome
    
    
    def as_dict(self, cover = True):
        return {
            "id": self.id,
            "view": "p",
            "nome": self.nome,
            "nome_f": self.nome,
            "cover": cover,
            "twidth": 200,
            "count": self.count,
            "list": [
                "nome",
                "count"
            ],
            "edit": [
                "nome"
            ]
        }


set_tags = {
    #"SourceFile": "name",
    #"File:FileName": "title",
    #"File:FileSize": "size",
    "File:MIMEType": "mime",
    "File:ImageWidth": "width",
    "File:ImageHeight": "height",
    "IPTC:Keywords": "keywords",
    "IPTC:Caption-Abstract": "description",
    "IPTC:Headline": "headline",
    "EXIF:Make": "make",
    "EXIF:Model": "model",
    "EXIF:Software": "software",
    "EXIF:Artist": "autore",
    "EXIF:Copyright": "copyright",
    "EXIF:ExposureTime": "exposure",
    "EXIF:FNumber": "fnumber",
    "EXIF:ExposureProgram": "program",
    "EXIF:ISO": "iso",
    "EXIF:DateTimeOriginal": "datetime",
    "EXIF:OffsetTimeOriginal": "timezone",
    "EXIF:ExposureCompensation": "compensation",
    "EXIF:MeteringMode": "metering",
    "EXIF:Flash": "flash",
    "EXIF:FocalLength": "focal",
    "EXIF:ExposureMode": "mode",
    "EXIF:WhiteBalance": "wb",
    "EXIF:SerialNumber": "serial",
    "EXIF:LensInfo": "lensinfo",
    "EXIF:LensModel": "lensmodel",
    "XMP:ImageNumber": "number",
    "XMP:ColorTemperature": "temperature",
    "XMP:Tint": "tint"
}

class File(models.Model):
    def upload_path(self, filename):
        return self.evento.folder + filename
    
    #image = models.FileField(upload_to='', null=True)
    evento = models.ForeignKey(Evento, on_delete=models.CASCADE, null=True)
    file = models.ImageField(upload_to=upload_path, null=True)
    #utente = models.ForeignKey(Utente, models.SET_NULL, blank=True, null=True)
    utente = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    upload_time = models.DateTimeField(auto_now_add=True)
    cover = models.BooleanField(default=False)
    meta = models.BooleanField(default=False)
    
    #name = models.CharField(max_length=200, null=True)
    title = models.CharField(max_length=200, null=True)
    #size = models.CharField(max_length=10, null=True)
    mime = models.CharField(max_length=80, null=True)
    width = models.PositiveSmallIntegerField(null=True)
    height = models.PositiveSmallIntegerField(null=True)
    
    keywords = models.ManyToManyField(Keyword, blank=True)
    autore = models.ForeignKey(Autore, on_delete=models.SET_NULL, blank=True, null=True)
    
    description = models.CharField(max_length=500, null=True)
    headline = models.CharField(max_length=500, null=True)
    
    make = models.CharField(max_length=30, null=True)
    model = models.CharField(max_length=50, null=True)
    software = models.CharField(max_length=100, null=True)
    copyright = models.CharField(max_length=100, null=True)
    exposure = models.FloatField(null=True)
    fnumber = models.FloatField(null=True)
    program = models.PositiveSmallIntegerField(null=True)
    iso = models.PositiveIntegerField(null=True)
    datetime = models.DateTimeField(null=True)
    #timezone = models.CharField(max_length=5, null=True)
    compensation = models.FloatField(null=True)
    metering = models.PositiveSmallIntegerField(null=True)
    flash = models.PositiveSmallIntegerField(null=True)
    focal = models.FloatField(null=True)
    mode = models.PositiveSmallIntegerField(null=True)
    wb = models.BooleanField(null=True)
    serial = models.CharField(max_length=20, null=True)
    lensinfo = models.CharField(max_length=50, null=True)
    lensmodel = models.CharField(max_length=50, null=True)
    
    number = models.PositiveIntegerField(null=True)
    temperature = models.PositiveSmallIntegerField(null=True)
    tint = models.SmallIntegerField(null=True)
    
    #focus = models.CharField(max_length=20)
    #vr = models.CharField(max_length=5)
    
    #codec = models.CharField(max_length=10)
    #duration = models.CharField(max_length=10)
    #framerate = models.CharField(max_length=5)
    #audio = models.CharField(max_length=10)
    
    #class Meta:
    #    permissions = [
    #        ("upload_files", "Can upload files"),
    #    ]
    
    
    def __str__(self):
        return str(self.file)
    
    
    def del_file(self):
        logger.info("Remove File: " + self.file.name)
        os.remove(self.file.path)
    
    
    #@property
    def thumbnail(self, size = "x200", crp = "noop", qual = 80):
        return get_thumbnail(self.file, size, crop=crp, quality=qual)
    
    
    def webversion(self, type, size = "1920"):
        if not self.meta:
            self.set_meta()
        
        if self.width < self.height:
            size = "x" + size
        
        img = Image.open(os.path.join(settings.MEDIA_ROOT, self.thumbnail(size).name))
        base = os.path.join(settings.STATIC_ROOT, "watermark/", str(self.autore.id))
        
        if os.path.exists(base):
            watermark = os.path.join(base, type + ".png")
            watermark = os.path.join(base, "default.png") if not os.path.exists(watermark) else watermark
            watermark = Image.open(watermark).convert("RGBA")
            
            ih, iw = img.size
            wh, ww = watermark.size
            
            img.paste(watermark, (ih-int(ih/100*1)-wh, iw-int(iw/100*1)-ww), watermark)
        
        byte = BytesIO()
        img.save(byte, 'jpeg')
        
        return byte.getvalue()
    
    
    #@sync_to_async
    def set_meta(self):
        logger.info("Set Meta: " + str(self.id))
        
        from exiftool import ExifTool
        
        with ExifTool() as et:
            metadata = et.get_tags(set_tags, self.file.path)
        
        if metadata.get("IPTC:Keywords"):
            objs = []
            if isinstance(metadata.get("IPTC:Keywords"), list):
                for d in metadata["IPTC:Keywords"]:
                    #print(str(self.id), str(d))
                    k = Keyword.objects.get_or_create(nome=d)
                    objs.append(k[0])
                    if k[1]:
                        logger.info("New Keyword: " + d) 
            else:
                k = Keyword.objects.get_or_create(nome=metadata["IPTC:Keywords"])
                objs.append(k[0])
                if k[1]:
                    logger.info("New Keyword: " + metadata["IPTC:Keywords"]) 
            
            self.keywords.add(*objs)
        
        if metadata.get("EXIF:Artist"):
            a = Autore.objects.get_or_create(nome=metadata["EXIF:Artist"])
            if a[1]:
                logger.info("New Autore: " + metadata["EXIF:Artist"])
            self.autore = a[0]
        
        if metadata.get("EXIF:DateTimeOriginal"):
            metadata["EXIF:DateTimeOriginal"] = datetime.strptime(metadata["EXIF:DateTimeOriginal"], "%Y:%m:%d %H:%M:%S").isoformat(" ")
            
            if metadata.get("EXIF:OffsetTimeOriginal"):
                metadata["EXIF:DateTimeOriginal"] += metadata["EXIF:OffsetTimeOriginal"]
        else:
            logger.info("No DateTimeOriginal")
        
        #del metadata["IPTC:Keywords"]
        metadata.pop("IPTC:Keywords", None)
        metadata.pop("EXIF:Artist", None)
        metadata.pop("SourceFile", None)
        metadata.pop("EXIF:OffsetTimeOriginal", None)
        
        for k, v in metadata.items():
            setattr(self, set_tags[k], v)
            #print(str(set_tags[k]) + ": " + str(v))
        
        self.meta = True
        self.save()
    
    #def set_tipo(self):
    #    tipo = self.mime.split('/')[0]
    #
    #    if tipo == 'image':
    #        print("IMMAGINE")
    #    elif tipo == 'video':
    #        print("VIDEO")
    #    else:
    #        print("ALTRO")
    #
    #def get_video_thumb(self):
    #    import

    def as_dict(self, thumb = False):
        twidth = self.thumbnail(thumb).width if thumb else 0
        keywords_d, keywords_f, keywords_l = list_to_ne(self.keywords.all().values("id", "nome"), "nome")
        
        return {
            "id": self.id,
            "file": str(self.file),
            "evento": {
                "id": self.evento.id,
                "data": self.evento.data_inizio.strftime("%d/%m/%Y"),
                "nome": self.evento.nome,
                "luogo_f": self.evento.luogo_f
            },
            "mime": self.mime,
            #"data": self.datetime.strftime("%Y/%m/%d"),
            "data": self.datetime.strftime("%d/%m/%Y") if self.datetime else None,
            "keywords_f": keywords_f,
            "keywords": keywords_d,
            "autore": {
                "id": self.autore.id,
                "nome": self.autore.nome
            } if self.autore else {},
            "cover": False,
            "twidth": twidth
        }
        
    def meta_dict(self):
        def get_pass(v):
            return v
    
        def get_fraction(v):
            return str(Fraction(v).limit_denominator(10000))
    
        def get_program(v):
            return {
                1: "Manual",
                2: "Program AE",
                3: "Aperture-priority AE",
                4: "Shutter speed priority AE",
                5: "Creative (Slow speed)",
                6: "Action (High speed)",
                7: "Portrait",
                8: "Landscape",
                9: "Bulb"
            }.get(v, "Not Defined")
    
        def get_metering(v):
            return {
                1: "Average",
                2: "Center-weighted average",
                3: "Spot",
                4: "Multi-spot",
                5: "Multi-segment",
                6: "Partial"
            }.get(v, "Not Defined")
    
        def get_mode(v):
            return {
                0: "Auto",
                1: "Manual",
                2: "Auto bracket"
            }.get(v, "Not Defined")
        
        def get_lensinfo(v):
            v = v.split()
            
            if v:
                info = v[0] if v[0] == v[1] else v[0] + "-" + v[1]
                info += "mm f/"
                info += v[2] if v[2] == v[3] else v[2] + "-" + v[3]
                return info
            else:
                return ""
            
        def get_focal(v):
            return str(round(v)) + "mm" if v else ""
        
        def get_fnumber(v):
            return "f/" + str(v) if v else ""
        
        def get_flash(v):
            return "Flash" if v else ""
        
        def get_iso(v):
            return "ISO " + str(v) if v else ""
        
    
        tags_display = {
            "width": get_pass,
            "height": get_pass,
            "description": get_pass,
            "headline": get_pass,
            "make": get_pass,
            "model": get_pass,
            "software": get_pass,
            "copyright": get_pass,
            "exposure": get_fraction,
            "fnumber": get_fnumber,
            "program": get_program,
            "iso": get_iso,
            "datetime": get_pass,
            "compensation": get_fraction,
            "metering": get_metering,
            "flash": get_flash,
            "focal": get_focal,
            "mode": get_mode,
            "wb": get_pass,
            "serial": get_pass,
            "lensinfo": get_lensinfo,
            "lensmodel": get_pass,
            "number": get_pass,
            "temperature": get_pass,
            "tint": get_pass
        }
    
        meta = {
            "meta_list": ["model", "lensinfo", "focal", "fnumber", "exposure", "iso", "flash"]
        }
    
        for k in meta["meta_list"]:
            if getattr(self, k) != None:
                meta[k] = tags_display.get(k)(getattr(self, k))
    
        return meta


def user_as_dict(self):
    famiglie_d, famiglie_f, famiglie_l = list_to_ne(self.famiglia_set.all().values('id', 'nome'), 'nome')
    groups_d, groups_f, groups_l = list_to_ne(self.groups.all().values('id', 'name'), 'name')
    
    return {
        "id": self.id,
        "last_login": self.last_login,
        "is_superuser": self.is_superuser,
        "username": self.username,
        "nome_f": self.username,
        "first_name": self.first_name,
        "last_name": self.last_name,
        "email": self.email,
        "is_staff": self.is_staff,
        "is_active": self.is_active,
        "date_joined": self.date_joined,
        "famiglie_d": famiglie_d,
        "famiglie_f": famiglie_f,
        "famiglie": famiglie_l,
        "groups_d": groups_d,
        "groups_f": groups_f,
        "groups": groups_l,
        #"count": self.count,
        "list": [
            "username",
            "first_name",
            "last_name",
            "famiglie_f",
            "groups_f"
        ],
        "edit": [
            "username",
            "first_name",
            "last_name",
            "email",
            "groups",
            "famiglie"
        ]
    }

User.add_to_class("as_dict", user_as_dict)
