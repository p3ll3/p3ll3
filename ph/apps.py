from django.apps import AppConfig


class PhConfig(AppConfig):
    name = 'ph'
    
    def ready(self):
      import ph.signals